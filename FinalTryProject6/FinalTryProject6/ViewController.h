//
//  ViewController.h
//  FinalTryProject6
//
//  Created by Amanda Kobiolka on 4/23/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {

IBOutlet UILabel *numPairs;

IBOutlet UIButton *playAgainButton;
IBOutlet UIButton *button0;
IBOutlet UIButton *button1;
IBOutlet UIButton *button2;
IBOutlet UIButton *button3;
IBOutlet UIButton *button4;
IBOutlet UIButton *button5;
IBOutlet UIButton *button6;
IBOutlet UIButton *button7;
IBOutlet UIButton *button8;
IBOutlet UIButton *button9;
IBOutlet UIButton *button10;
IBOutlet UIButton *button11;
IBOutlet UIButton *button12;
IBOutlet UIButton *button13;
IBOutlet UIButton *button14;
IBOutlet UIButton *button15;

    
}
-(IBAction)flipCard:(id)sender;

@end

