//
//  ViewController.m
//  FinalTryProject6
//
//  Created by Amanda Kobiolka on 4/23/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@end

NSArray *cardArray;
NSArray *cardFrontArray;
NSInteger selectedCards[2];
NSInteger checkMatch[8];
int pairs;
int clickedCards;
bool twoSelected = false;

@implementation ViewController


-(IBAction)flipCard:(id)sender {
    NSLog(@"In flip card");
    for(int i = 0; i < 16; i++) {
        if (sender == cardArray[i]) {
            clickedCards++;
            NSLog(@"Deeper in flip card");
            NSLog(@"i is : %d", i);
            
            [cardArray[i] setBackgroundImage:cardFrontArray[i]
                                    forState:UIControlStateNormal];
         
            if(clickedCards == 1) {
                selectedCards[0] = i;
            } else if (clickedCards == 2){
                selectedCards[1] = i;
                twoSelected = true;
                [self checkForMatch];
            }
        }
    }
}


- (IBAction)playAgain:(id)sender {
    numPairs = 0;
    NSString* numPairsStr = [NSString stringWithFormat:@"Number of pairs: %i", pairs];
    numPairs.text = numPairsStr;
    
    for(int i = 0; i < 16; i++) {
        
        [cardArray[i] setBackgroundImage:[UIImage imageNamed:@"cardback.png"]
                                forState:UIControlStateNormal];
        
    }
    
    playAgainButton.hidden = NO;
}

-(void) checkForMatch {
            
    //[NSThread sleepForTimeInterval: 3.0];
    
                clickedCards = 0;
                NSLog(@" If the count equals 2");
                
                int t = selectedCards[0];
                int t1 = selectedCards[1];
                
                if (t < 8) {
                    NSLog(@" If the count equals 2, and t is less than 8");

                    if (checkMatch[t] == t1) {
                        NSLog(@" We have a match");

                        selectedCards[0] = 0;
                        selectedCards[1] = 0;
                        
                        pairs++;
                        NSString* numPairsStr = [NSString stringWithFormat:@"Number of pairs: %i", pairs];

                        numPairs.text = numPairsStr;
                        
                        if (pairs == 8) {
                            NSLog(@"Are we even getting here?");
                            playAgainButton.hidden = NO;

                        }
                        

                    
                    } else {
                        [cardArray[t] setBackgroundImage:[UIImage imageNamed:@"cardback.png"]
                                            forState:UIControlStateNormal];
                        [cardArray[t1] setBackgroundImage:[UIImage imageNamed:@"cardback.png"]
                                            forState:UIControlStateNormal];
                        twoSelected = false;
                    
                    }
                } else {
                    NSLog(@" If the count equals 2, and t is 8 or greater");
                    NSLog(@" t1 %d", t1);
                    NSLog(@" t %d", t);

                    if (checkMatch[t1] == t) {
                        NSLog(@" We have a match");
                        
                        selectedCards[0] = 0;
                        selectedCards[1] = 0;
                        
                        pairs++;
                        NSString* numPairsStr = [NSString stringWithFormat:@"Number of pairs: %i", pairs];
                        
                        numPairs.text = numPairsStr;
                        if (pairs == 8) {
                            NSLog(@"Are we even getting here?");

                            playAgainButton.hidden = NO;
                            
                        }
                        
                    } else {
                        

                        [cardArray[t] setBackgroundImage:[UIImage imageNamed:@"cardback.png"]
                                                forState:UIControlStateNormal];
                        [cardArray[t1] setBackgroundImage:[UIImage imageNamed:@"cardback.png"]
                                                 forState:UIControlStateNormal];
                        twoSelected = false;
                        
                    }
                    
                    
                }
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"hyrule.jpg"]];
// Do any additional setup after loading the view, typically from a nib.
    playAgainButton.hidden = YES;

    
    UIImage *card0 = [UIImage imageNamed:@"darkLink.png"];
    UIImage *card1 = [UIImage imageNamed:@"skullKid.png"];
    UIImage *card2 = [UIImage imageNamed:@"ganondorf.png"];
    UIImage *card3 = [UIImage imageNamed:@"goron.png"];
    
    UIImage *card4 = [UIImage imageNamed:@"epona.png"];
    UIImage *card5 = [UIImage imageNamed:@"link.png"];
    UIImage *card6 = [UIImage imageNamed:@"shield.png"];
    UIImage *card7 = [UIImage imageNamed:@"zelda.png"];
    
    UIImage *card8 = [UIImage imageNamed:@"darkLink.png"]; //8
    UIImage *card9 = [UIImage imageNamed:@"ganondorf.png"]; //9
    UIImage *card10 = [UIImage imageNamed:@"goron.png"]; //10
    UIImage *card11 = [UIImage imageNamed:@"skullKid.png"];//11
    
    UIImage *card12 = [UIImage imageNamed:@"epona.png"];//12
    UIImage *card13 = [UIImage imageNamed:@"shield.png"];//13
    UIImage *card14 = [UIImage imageNamed:@"zelda.png"];//14
    UIImage *card15 = [UIImage imageNamed:@"link.png"];//15
    
    
    checkMatch[0] = 8;
    checkMatch[1] = 11;
    checkMatch[2] = 9;
    checkMatch[3] = 10;
    
    checkMatch[4] = 12;
    checkMatch[5] = 15;
    checkMatch[6] = 13;
    checkMatch[7] = 14;
    
    
    cardFrontArray = [[NSArray alloc] initWithObjects:card0, card1,card2,card3,card4, card5, card6, card7, card8, card9, card10, card11, card12, card13, card14, card15, nil];

    cardArray = [[NSArray alloc] initWithObjects:button0, button1,button2,button3,button4, button5,button6,button7,button8, button9,button10,button11,button12, button13,button14,button15,nil];

    for(int i = 0; i < 16; i++) {
        
         [cardArray[i] setBackgroundImage:[UIImage imageNamed:@"cardback.png"]
                          forState:UIControlStateNormal];
         [cardArray[i] addTarget:self
                          action:@selector(flipCard:)
         forControlEvents:UIControlEventTouchUpInside];

    }
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
